# Data Science CI Example

此 Project 的內容來自於 GitLab 原廠的 [Data Science CI Example](https://gitlab.com/gitlab-data/data-science-ci-example) 並稍作改編，僅留下在 iThome Cloud Summit 20204 「Lab：輕輕鬆鬆體驗MLOps (with GitLab)」 所需的內容。

Lab 的操作步驟，可以參閱我的 [Blog 文章 - Lab：輕輕鬆鬆體驗 MLOps（with GitLab）](https://chengweichen.com/2024/07/ithome-cloud-summit-2024-mlops-with-gitlab.html)

有興趣體驗原廠原版 Example 的朋友，可以前往原廠的 [Project](https://gitlab.com/gitlab-data/data-science-ci-example) ，依據 README.md 的內容操作。

