FROM nvcr.io/nvidia/cuda:12.1.1-base-ubuntu22.04
LABEL maintainer="dietz"
COPY .    /app/
WORKDIR /app
RUN apt-get update
RUN apt-get install -y python3.9
RUN apt-get install -y python3-pip
RUN pip install -r requirements.txt


